<?php

namespace mrimaster\articler\services;

/**
 * Description of Utils
 *
 * @author kuro
 */
class Utils {
   
    /**
     * Adds to $array pairs with keys from $list
     * and values from the same fields of $object
     * Creates $array if it's not specified
     * @param Object $object contents of data
     * @param Array $list list parameter names
     * @param Array $array target array (empty by default)
     * @return Array $array
    */
    public static function fillParameters($object, $list, $array = []) {
        foreach($list as $name) {
            $array[$name] = $object->$name;
        }
        
        return $array;
    }
    
   
    /**
     * Make from the $text string with $words words
     * @param String $text original big text
     * @param Integer $words numbers of words in short text
     * @return String short text
    */
    public static function shortText($text, $words = 11) {
        return implode(' ', array_slice(explode(' ', $text), 0, $words));
    }
}
