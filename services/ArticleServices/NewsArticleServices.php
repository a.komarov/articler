<?php

namespace mrimaster\articler\services\ArticleServices;

use mrimaster\articler\models\Article;
use mrimaster\articler\services\ArticleServices;


/**
 *
 * @author kuro
 */
trait NewsArticleServices {
    
    private static function makeNews($articles) {
        $news =[];
        
        foreach($articles as $article) {
            $news[] = ArticleServices::getParams($article->href, [
                'title', 'breadcrumbs','keywords', 'date_created',
                'image' => [
                    'prefix' => 'http://mrt-kt.ru.articles.s3.amazonaws.com/',
                    'suffix' => '.jpg'
                ]]);
        }
        
        return $news;
    }
    
    /**
     * Returns last $count created articles
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array last news
    */
    public static function lastnews ($article = NULL, $params = []) {
        $count = isset($params['count']) ? $params['count'] : 5;
        $articles = Article::find()->orderBy(['date_created' => SORT_DESC, 'hidden' => 0])->limit($count)->all();
        
        return self::makeNews($articles);
    }
    
    /**
     * Returns $count the most popular articles
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array articles
    */
    public static function topnews ($article = NULL, $params = []) {
        $count = isset($params['count']) ? $params['count'] : 5;
        $articles = Article::find()->orderBy(['version' => SORT_DESC, 'hidden' => 0])->limit($count)->all();
        
        return self::makeNews($articles);
    }
    
    /**
     * Returns $count the most popular articles
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array articles
    */
    public static function childrennews ($article, $params = []) {
        
        //return self::lastnews (NULL, ['count' => 19]); //заглушка
        
        $parent = is_string($article) ? Article::find()->where(['href' => $article])->one() : $article;
        
        $articles = Article::find()->where(['parent_id' => $parent->id, 'hidden' => 0])->all();
        
        
        return self::makeNews($articles);
    }
}
