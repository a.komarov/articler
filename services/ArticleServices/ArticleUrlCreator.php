<?php


namespace mrimaster\articler\services\ArticleServices;

use mrimaster\articler\models\Article;
/**
 * Description of ArticleUrlCreator
 *
 * @author kuro
 */
trait ArticleUrlCreator  {
    
    /**
     * NestedUrlCreator interface
     * Make valid url for an article
     * @param string $href end point of the url
     * @return string url
    */

    public static function getUrl($href) {
        //to avoid errors below
        if (!\mrimaster\articler\services\ArticleServices::toShow($href)) {
            return $href;
        }
        
        $node = Article::find()->where(['href' => $href])->one();
        
        $breadcrumbs = [$node->href];
        
        while($node->parent_id != NULL) {
            $node = Article::find()->where(['id' => $node->parent_id])->one();
            if (count($node->href)>0)
                $breadcrumbs[] = $node->href;
        }
        
        return implode('/', array_reverse($breadcrumbs));
    }
}
