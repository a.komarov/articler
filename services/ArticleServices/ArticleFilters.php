<?php

namespace mrimaster\articler\services\ArticleServices;

use yii\helpers\Url;
/**
 * Description of ArticleFilters
 *
 * @author kuro
 */
trait ArticleFilters {
    
    /**
     * In $text replaces [B|A] with hyperlinks with text A and url B from 
     * @param string $text original text
     * @return string text with hyperlinks
    */
    private static function shortcodes($text) {
        
        $filter = function ($matches) {
            $url = Url::toRoute(['site/article', 'href' => $matches[1]]);
            $text = $matches[2];
            return "<a href=\"$url\">$text</a>";
        };
        
        return preg_replace_callback("/\[(.*?)\|(.*?)\]/i", $filter, $text);
    }
    
    /**
     * In $text to <img> src adds prefix and suffix and classes
     * @param string $prefix src prefix
     * @param string $suffix src suffix
     * @param string $class classes, separated with spaces
     * @param string $text original text
     * @return string text with edited images
    */
    private static function images($text, $params) {
        
        $prefix = $params['prefix'];
        $suffix = $params['suffix'];
        //$class = $params['class'];
        
        //$class = '" class ="' . $class;
        return preg_replace("/(<img[^>]*src *= *[\"']?)([^\"']*)/i", "$1 $prefix$2$suffix", $text);
    }
    
}
