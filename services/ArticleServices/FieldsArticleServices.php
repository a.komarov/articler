<?php

namespace mrimaster\articler\services\ArticleServices;

/**
 * Description of FieldsArticleServices
 *
 * @author kuro
 */
trait FieldsArticleServices {
    
    //Deprecated
    /*
    private static function title($article = NULL, $params = []) {
        return $article->title;
    }
    
    private static function name($article = NULL, $params = []) {
        return $article->name;
    }
    
    private static function keywords($article = NULL, $params = []) {
        return $article->keywords;
    }
    
    private static function description($article= NULL, $params = []) {
        return $article->description;
    } 
    
    private static function last_updated($article= NULL, $params = []) {
        return $article->last_updated;
    }
    
    private static function date_created($article= NULL, $params = []) {
        return $article->date_created;
    }
    */
    
    private static function text($article = NULL, $params = []) {
        $text = $article->text;
        
        if (isset($params['filters'])) {
            $filters = $params['filters'];
            foreach($filters as $name=>$filter) {
                if (is_string($filter)) {
                    $text = self::$filter($text);
                } else  {
                    $text = self::$name($text, $filter);
                }
            }
        }
        
        return $text;
    }
    
    
    private static function image($article = NULL, $params = []) {
        
        if (!isset($article->image))
            return "";
        
        $prefix = $params['prefix'];
        $suffix = $params['suffix'];
        $image = $article->image;
        
        return "$prefix$image$suffix";
    }
}
