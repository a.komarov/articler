<?php

namespace mrimaster\articler\services\ArticleServices;

use mrimaster\articler\models\Article;
use yii\helpers\Url;

/**
 * Description of SitemapArticleServices
 *
 * @author kuro
 */
trait SitemapArticleServices {
    
    /**
     * Return sitemap as array
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array sitemap
    */
    private static function sitemap($article = NULL, $params = []) {
        $host = \Yii::$app->request->hostInfo;
        
        $articles = Article::find()->where(['hidden' => 0])->all();
        
        $urls = [];
        foreach($articles as $article) {
            $urls[] = [
                'loc' => "$host" . Url::toRoute(['site/index', 'href' => $article->href]),
                'lastmod' => date("Y-m-d", strtotime($article->last_updated)),
                'changefreq' => 'weekly',
                'priority' => 1,
            ];
        }
        
        return  $urls;
    }
    
}
