<?php

namespace mrimaster\articler\services\ArticleServices;

use mrimaster\articler\models\Article;

/**
 * Description of NavigationArticleServices
 *
 * @author kuro
 */
trait NavigationArticleServices {
    
    /**
     * Make breadcrumbs 
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array tree
    */
    private static function breadcrumbs($article = NULL, $params = []) {
        $TOPfilter = function ($label) {
            return $label == 'TOP' ? "" : $label;
        };
        $breadcrumbs = [];
        do {
            $breadcrumbs[] = ['name' => $TOPfilter($article->name), 'href' => $article->href];
            $article = Article::find()->where(['id' => $article->parent_id])->one();
        } while($article->parent_id != NULL);
        
        return array_reverse($breadcrumbs);
    }
    
    
    /**
     * Return a tree of articles
     * @param $article object of article or href
     * @param $params array of parameters for service method
     * @return Array tree
    */
    private static function children($article, $params) {
        
        $parent = is_string($article) ? Article::find()->where(['href' => $article])->one() : $article;
        
        $query = Article::find()->where(['parent_id' => $parent->id, 'hidden' => 0])->orderBy('position');
                
        if (!$query->exists()) {
            return [];
        }
        
        $articles = $query->all();
        
        $nodes = [];
        
        foreach( $articles  as  $articler ) {
            $node = [
                'name' => $articler->name,
                'href' => $articler->href
            ];
            
            if (isset($params['breadcrumbs']))
                //we can pass $node here only while we have only name and href
                $node['active'] = in_array($node, $params['breadcrumbs']);
            
            //recursive call to children
            $node['list'] = self::children($articler, $params);
            
            $nodes[] = $node;
        }
        
        return $nodes;
    }
    
    /**
     * Make navigation (top links or whole tree)
     * @param $article object of article of href
     * @param $params array of parameters for service method
     * @return Array tree
    */
    private static function navigation($article = NULL, $params = [
        'parent' => false
    ]) {
        
        $breadcrumbs = self::breadcrumbs($article);
        
        if (!isset($params['parent']) || !$params['parent']) {
            $parent = Article::findOne(['name' => 'TOP']) ; //root
            return self::children($parent, ['breadcrumbs' => $breadcrumbs]);
        }
        
        $parent = Article::findOne(['href' => $params['parent']]) ;
        
        
        $node = [
            'name' => $parent->name,
            'href' => $parent->href
        ];
        $node['active'] = in_array($node, $breadcrumbs);
        $node['list'] = self::children($parent->href, ['breadcrumbs' => $breadcrumbs]);
        
        return $node;            
        
        
    }
}
