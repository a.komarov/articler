<?php

namespace mrimaster\articler\services;

use mrimaster\articler\models\Article;
//use mrimaster\articler\rules\NestedUrlCreator;

/**
 * Description of ArticleServices
 *
 * @author kuro
 */

class ArticleServices //implements NestedUrlCreator 
{
    //main AutoCall method to call services
    use \mrimaster\articler\services\AutoCall;
    
    use \mrimaster\articler\services\ArticleServices\NavigationArticleServices;
    use \mrimaster\articler\services\ArticleServices\SitemapArticleServices;
    use \mrimaster\articler\services\ArticleServices\FieldsArticleServices;
    use \mrimaster\articler\services\ArticleServices\ArticleUrlCreator;
    use \mrimaster\articler\services\ArticleServices\ArticleFilters;
    use \mrimaster\articler\services\ArticleServices\NewsArticleServices;
    
    /**
     * You can get parameters for article view with this method 
     * @param String $href href (last slug in url) of the article
     * @param Array $services array of services names and parameters for them
     * @return Array parameters
    */
    public static function getParams($href, $services, $params = []) {
        $params['href'] = $href;
        
        //Get article from the db just once
        $query = Article::find()->where(['href' => $href]);
        $article = ($query->exists()) ? $query->one() : $href;
        
        return self::autoCall($article, $services, $params);
    }
    
    public static function toShow($href) {
        $query = Article::find()->where(['href' => $href, 'hidden' => 0]);
        return $query->exists();
    }
    
}
