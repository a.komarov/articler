<?php

namespace mrimaster\articler\services;

/**
 * Description of AutoCall
 *
 * @author kuro
 */
trait AutoCall {
    
    public static function autoCall($object, $services, $params = []) {
        
        //We will call all service methods for view parameters
        //and collect them to $params array
        foreach($services as $name=>$service) {
            $service_name = is_array($service) ? $name : $service;
            $service_params  = is_array($service) ? $service : NULL;
            
            //Check if there is service
           if (is_callable("self::$service_name")) {
                           //Call service with params if they are passed
                $params[$service_name] = isset($service_params) ? 
                    self::$service_name($object, $service_params) :
                    self::$service_name($object) ;
            } 
            //or it's just a field of object
            else {
                $params[$service_name] = $object->$service_name;
            } 
        }
        
        return $params;
    }
    
    
    
}
