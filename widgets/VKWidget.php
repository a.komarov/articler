<?php

namespace mrimaster\articler\widgets;

/**
 * Description of VKWidget
 *
 * @author kuro
 */
class VKWidget {
    
    public static function show($apiId) {
        return "
            VK.init({apiId: $apiId, onlyWidgets: true});
            VK.Widgets.Like('vk_like', {type: 'button'});";
    }
}
