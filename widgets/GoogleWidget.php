<?php

namespace mrimaster\articler\widgets;

/**
 * Description of GoogleWidget
 *
 * @author kuro
 */
class GoogleWidget {
    
    public static function show() {
        return "
            (function () {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js?onload=onLoadCallback';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();";
    }
}
