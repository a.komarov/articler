<?php

namespace mrimaster\articler\widgets;

use yii\helpers\Url;
/**
 * Description of NavigationWidget
 *
 * @author kuro
 */
class NavigationWidget {
    
    public static function showChildren($nav) {
        if (!$nav['active']) return "";
        
        $code = '<ul class="sub" style="display: block">';
        foreach($nav['list'] as $secondLevelArticle) {
            
            $active_class = $secondLevelArticle['active'] ? "active" : "";
            $route = Url::toRoute(['site/index', 'href' => $secondLevelArticle['href']]);
            $name = $secondLevelArticle['name'];
            
            
            $carrotdown = (count($secondLevelArticle['list']) > 0) ? 
                    '<span class="menu-arrow arrow_carrot-down"></span>' : '';
            
            $code .= "<li class=\"$active_class\">
                        <a href=\"$route\">
                            $name
                            $carrotdown
                        </a>
                      </li>";
            
            /*if (count($secondLevelArticle['list']) > 0)
                $code .= '<span class="menu-arrow arrow_carrot-down"></span>';*/
            foreach($secondLevelArticle['list'] as $thirdLevelArticle) {
                $active_class = $secondLevelArticle['active'] ? "active" : "";
                $route = Url::toRoute(['site/index', 'href' => $thirdLevelArticle['href']]);
                $name = $thirdLevelArticle['name'];
                
                $code .=  "<li class=\"$active_class\">
                    <a href=\"$route\">- $name</a>
                </li>";
            }
        
        }
        
        $code .= '</ul>';
        
        return $code;
    }
    
    public static function show($nav,$icon) {
        $active_class = $nav['active'] ? "active" : "";
        $route = Url::toRoute(['site/index', 'href' => $nav['href']]);
        $name = $nav['name'];
        $children = self::showChildren($nav);
        
        return "
        <li class=\"sub-menu $active_class\">
            <a href=\"$route\">
                <i class=\"$icon\"></i>
                <span>$name</span>
                <span class=\"menu-arrow arrow_carrot-right\"></span>
            </a>
            $children
        </li>";
    }
    
    
}
