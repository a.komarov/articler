<?php

namespace mrimaster\articler\widgets;

class SitemapWidget {
    
    private static function getUrlset($models) {
        $urlset = [];
        
        foreach($models as $model) {
            $class = 'app\services\\' . ucfirst($model) . 'Services';
            $urlset = array_merge($urlset, $class::getParams(NULL, ['sitemap'])['sitemap']);
        } 
        
        return $urlset;
    }
    
    public static function show($models) {
        
        $xml = "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";
        foreach(self::getUrlset($models) as $url) {
            extract($url);
            $xml .= "
                <url>
                    <loc>$loc</loc>
                    <lastmod>$lastmod</lastmod>
                    <changefreq>$changefreq</changefreq>
                    <priority>$priority</priority>
                </url>";
        }
        $xml .= "</urlset>";
        
        return $xml;
    }
}

