<?php

namespace mrimaster\articler\widgets;

/**
 * Description of YandexMapsWidget
 *
 * @author kuro
 */
class YandexMapsWidget {
    //put your code here
    
    private static $declareCodes = [
        'addPoint' => "
            function addPoint(coord, name, link, phone, type, color) {

                var myPlacemark = new ymaps.Placemark(coord, {
                    balloonContentHeader: '<a href=\"' + link + '\">' + name + '</a>',
                    balloonContentFooter: phone,
                    hintContent: name
                }, {
                    preset: 'islands#' + type
                });
                map.geoObjects.add(myPlacemark);
            }
            "
    ];
    
    private static function declares($declares) {
        $declareCode = "";
        foreach($declares as $name) {
            $declareCode .= self::$declareCodes[$name];
        }
        
        return $declareCode;
    }
    
    private static function render($text, $declareCodes = [], $params = ['show_tag' => false]) {
        
        $declareCode = self::declares($declareCodes);
        
        return ($params['show_tag'] ? "<script>" : "") .
            "ymaps.ready(
                function () {
                    $text 
                }
            );
            $declareCode"
            . ($params['show_tag'] ? "</script>" : "")
            ;
    }


    public static function clinics($clinics, $params) {
        $zoom = isset($params['zoom']) ? $params['zoom'] : 10;
        $longitude = isset($params['center']) ? $params['center']['longitude'] : 59.939095;
        $latitude = isset($params['center']) ? $params['center']['latitude'] : 30.315868;
        $start =   "map = new ymaps.Map('map', {
                        center: [$longitude, $latitude],
                        zoom: $zoom,
                        behaviors: ['default'],
                        controls: ['zoomControl', 'rulerControl']
                    });
                    map.behaviors.disable(['scrollZoom']);
                    ";
        
        $addPointCode = "";
        foreach($clinics as $clinic) {
            
            if (is_array($clinic))
                $clinic = (object)$clinic;
            
            $longitude = $clinic->longitude;
            $latitude = $clinic->latitude;
            $name = $clinic->name;
            $id_str = $clinic->id_str;
            $phone = $clinic->phone;
            $dotIcon = $clinic->private_clinic ? "blueDotIcon" : "grayDotIcon";
            
            $addPointCode .= "addPoint([ $longitude , $latitude], '$name', 
                    '/clinic/$id_str', '$phone', '$dotIcon' , 'blue');
                    ";
        
        }
        
        $code = $start . $addPointCode;
        
        return self::render($code, ['addPoint'], $params);
    }
    
    public static function clinic($clinic, $params) {
        if (is_array($clinic))
                $clinic = (object)$clinic;
        
        $name = $clinic->name;
        $phone = $clinic->phone;
        $longitude = $clinic->longitude;
        $latitude = $clinic->latitude;
        
        $zoom = isset($params['zoom']) ? $params['zoom'] : 15;
        
        $code = "var map = new ymaps.Map('map', {
                    center: [59.939095, 30.315868],
                    zoom: $zoom ,
                    behaviors: ['default']
                });
                map.controls.add('mapTools')
                    .add('zoomControl')
                    .add('typeSelector');

                var coord = [ $longitude , $latitude];
                var myPlacemark = new ymaps.Placemark(coord, {
                    balloonContentHeader: '$name<br/>$phone',
                    hintContent: '$name'
                });
                map.geoObjects.add(myPlacemark);
                map.setCenter(coord);
                myPlacemark.balloon.open();
                ";
        
        return self::render($code, [], $params);
                
    }
}
