<?php

namespace mrimaster\articler\widgets;

use yii\helpers\Url;

/**
 * Description of InfoMrtPaginationWidget
 *
 * @author kuro
 */
class InfoMrtPaginationWidget {    
    public static function show($href, $news, $page, $perpage = 6) {
        //page = 1
        
        $count = count($news); //19
        $pages = ceil($count / $perpage); //16/6 = 4
        //die($pages);
        $nextpage = $page + 1; //2
        $prevpage = $page - 1; //0
        $start = ($page-1) * $perpage; //0
        $finish = $page * $perpage < $count ? 
                    $page * $perpage : $count;
        
        
        if ($page > $pages) return "";
        
        //start
        $code = "<div id='dle-content'>";
        
        for($i = $start; $i < $finish; $i++) {
            $new = $news[$i];
            
            $image = $new['image'];
            $url = Url::toRoute(['site/article', 'href' => $new['href']]);
            $title = $new['title'];
            $parent = $new['breadcrumbs'][0]['name'];
            
            $code .= 
                "<div class='shortstory'>
                    <div class='zoomimage'><div class='img' style='background-image:url($image);'></div></div>
                    <a href='$url'>$title</a>
                    <p><i class='fa fa-th'></i> $parent</p>
                </div>";
        }
        
        
            //pagination start
            $code .= '<div class="pagination">';
                //prev
                $code .= '<span class="b">';
                $prevurl = Url::toRoute(['site/article', 'href' => $href, 'page' => $prevpage]);
                $code .= $page > 1 ? "<a href='$prevurl'>" :'<span>';
                $code .= '<i class="fa fa-angle-left"></i>';
                $code .= $page > 1 ? "</a>" :'</span>';
                $code .= '</span>';
                //pages
                for($i=1; $i<= $pages; $i++) {
                    $url = Url::toRoute(['site/article', 'href' => $href, 'page' => $i]);
                    $code .= ($i == $page) ? 
                        "<span>$i</span>" :
                        "<a href='$url'>$i</a>";
                }
                //next
                $code .= '<span class="b">';
                $nexturl = Url::toRoute(['site/article', 'href' => $href, 'page' => $nextpage]);
                $code .= $page <= $pages ? "<a href='$nexturl'>" :'<span>';
                $code .= '<i class="fa fa-angle-right"></i>';
                $code .= $page <= $pages ? "</a>" :'</span>';
                $code .= '</span>';
            //pagination end
            $code .= '</div>';
        //end
        $code .= '</div>';
        
        return $code;
    
    }
}
