<?php

namespace mrimaster\articler\widgets;

/**
 * Description of YandexMetricsWidget
 *
 * @author kuro
 */
class YandexMetricsWidget {
    
    public static function show($id) {
        return "
        <noscript><div><img src=\"//mc.yandex.ru/watch/$id\" style=\"position:absolute; left:-9999px;\" alt=\"\" /></div></noscript>
        <script>
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter$id = new Ya.Metrika({id: $id,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                  w.yaCounter = w.yaCounter$id;
                } catch(e) { }
            });

            var n = d.getElementsByTagName('script')[0],
                s = d.createElement('script'),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = 'text/javascript';
            s.async = true;
            s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js';

            if (w.opera == '[object Opera]') {
                d.addEventListener('DOMContentLoaded', f, false);
            } else { f(); }
        })(document, window, 'yandex_metrika_callbacks');
    </script>";
    }
}
