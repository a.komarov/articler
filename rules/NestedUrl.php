<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mrimaster\articler\rules;

/**
 * Interface for NestedUrlRule
 * Use getUrl to define an url of hierarchical slugs
 * with $slug in the end
 * @author kuro
 */

interface NestedUrl {
    
    /**
     * Returns valid url for a slug
     * @param string $slug slug
     * @return string url
    */
    function getUrl($slug);
    
    /**
     * This method should throw HTTP exceptions if there any for this slug
     * @param string $slug slug
    */
    function check($slug);
    
    /**
     * Returns route for slug proccessing
     * @return string route
    */
    function default_route();
    
    /**
     * Returns route for redirecting on wrong url
     * @return string route
    */
    function redirect_route();
    
    /**
     * Returns return slug for \ url
     * @return string home slug
    */
    function home_slug();
    
    /**
     * Returns name of parameter in route which consist slug
     * @return string slug param name
    */
    function slug_param();
}
