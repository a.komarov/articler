<?php

namespace mrimaster\articler\rules;

/**
 * Description of LineUrl
 *
 * @author kuro
 * 
 * Basic class, that supports operating with slug1/slug2/.../slug urls.
 * You should extend this class and use initialize method in new class' 
 * constructor to set settings
 */

trait NestedUrlPaginatedRule {
        
    public function createUrl($manager, $route, $params) {
        if (!isset($params[self::slug_param()]))
            return "";
        $slug = $params[self::slug_param()];
        
        //don't make '/home' url
        if ($slug == self::home_slug())
            return '';
        
        $url = $this->getUrl($slug);
        //if (substr($url, 0, 1) == '/') $url = substr($url, 1);
        if (isset($params['page']) && $params['page'] > 1)
            $url .= "/page/" . $params['page'];
        
        return $url;
    }

    public function parseRequest($manager, $request) {
        
        //default route
        if ($request->url == "/") 
            return [self::default_route(),[]];
        
        $params = [];
        
        //separate */page/* parts
        //die(var_dump());
        $matches = explode("/page/", $request->url);
        $requestUrl = $matches[0];
        
        //save page if we have it
        if (isset($matches[1]) /*&& is_int($matches[1])*/)
            $params['page'] = $matches[1];
        
        //extract last slug in url
        $slug = self::getSlug($requestUrl);
        $params[self::slug_param()] = $slug;
        
        //if don't exist - 404
        self::check($slug);
        
        //get valid url
        $url = '/' . $this->getUrl($slug);
        if (isset($params['page']))
            $url .= "/page/" . $params['page'];
        
        //compare and return
        if ($request->url == $url)
            return [self::default_route(), $params];
        else
            return [self::redirect_route(),['url'=>$url]];
    }
    
    protected function getSlug($url) {
        $slugs = explode('/', $url);
        $last = count($slugs) - 1;
        $slug = $slugs[$last];
        while ($slug == '/')
            $slug = $slugs[--$last];
        
        return $slug;
    }
    
    /**
     * Returns valid url for a slug
     * @param string $slug slug
     * @return string url
    */
    protected function getUrl($slug) {
        throw new \Exception('Redefine getUrl method in ' . static::class . "\n
            getUrl must return correct url for this slug
        ");
    }
    
    /**
     * This method should throw HTTP exceptions if there any for this slug
     * @param string $slug slug
    */
    protected function check($slug) {
        throw new \Exception('Redefine check method in ' . static::class . "\n
            It should throw an http exception on wrong slugs
        ");
    }
    
    /**
     * Returns route for slug proccessing
     * @return string route
    */
    protected function default_route() {
        throw new \Exception('Redefine default_route method in ' . static::class . "\n
            default_route must return route for proccessing slug
        ");
    }
    
    /**
     * Returns route for redirecting on wrong url
     * @return string route
    */
    protected function redirect_route() {
        throw new \Exception('Redefine redirect_route method in ' . static::class . "\n
            redirect_route must return route for redirecting on wrong url
        ");
    }
    
    /**
     * Returns return slug for \ url
     * @return string home slug
    */
    protected function home_slug() {
        throw new \Exception('Redefine home_slug method in ' . static::class . "\n
            home_slug return slug for '\\' url
        ");
    }
    
    /**
     * Returns name of parameter in route which consist slug
     * @return string slug param name
    */
    protected function slug_param() {
        throw new \Exception('Redefine slug_param method in ' . static::class . "\n
            slug_param return name of parameter in route which consist slug
        ");
    }
}
