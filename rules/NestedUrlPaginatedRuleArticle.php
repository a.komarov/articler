<?php

namespace mrimaster\articler\rules;

use yii\web\UrlRuleInterface;
use mrimaster\articler\services\ArticleServices;
/**
 * Description of NestedArticleUrl
 *
 * @author kuro
 */
class NestedUrlPaginatedRuleArticle implements UrlRuleInterface/*, NestedUrl*/ {
   
    use NestedUrlPaginatedRule;
    
    protected function getUrl($href) {
        return ArticleServices::getUrl($href);
    }
    
    protected function check($href) {
        if (!ArticleServices::toShow($href)) {
            throw new \yii\web\NotFoundHttpException("Такой статьи не существует", 404);
        }
    }
    
    protected function default_route() {
        return 'site/article';
    }
    protected function redirect_route() {
        return 'site/redirect';
    }
    
    protected function home_slug() {
        return 'home';
    }
    
    protected function slug_param() {
        return 'href';
    }
}
