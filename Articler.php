<?php

namespace mrimaster\articler;

/**
 * Description of Articler
 *
 * @author kuro
 */
class Articler extends \yii\base\Module
{
    public $controllerNamespace = 'mrimaster\articler\controllers';
    
    public function init()
    {
        parent::init();

        //$this->params['foo'] = 'bar';
        // ... остальной инициализирующий код ...
    }
}