<?php

namespace mrimaster\articler\controllers;

use yii\web\Controller;

class SitemapController extends Controller
{
    
    public function actionIndex()
    {   
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->headers->add('Content-Type', 'text/xml');
        
        return \mrimaster\articler\widgets\SitemapWidget::show(\Yii::$app->params['sitemap']['models']);
    }
}