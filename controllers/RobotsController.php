<?php

namespace mrimaster\articler\controllers;

use yii\web\Controller;

class RobotsController extends Controller
{
    
    public function actionIndex()
    {   
        $host = \Yii::$app->request->hostInfo;
        return "User-agent: *\n"
                . "Allow: /\n"
                . "Sitemap: $host/sitemap.xml";
    }
}